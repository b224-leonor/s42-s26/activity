const express = require("express");
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require("../auth");

router.post("/addProduct", (req, res) => {

	const adminUser = auth.decode(req.headers.authorization)

	if(adminUser.isAdmin){
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send({auth: "failed"})
	}
});

router.get("/allProducts", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController))
});


module.exports = router;