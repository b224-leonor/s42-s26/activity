const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId : {
		type: mongoose.SchemaTypes.ObjectId,
		ref: "User"
			},
	products: [
			{
				productId: {
					type: mongoose.SchemaTypes.ObjectId,
					ref: "Product"
				},
				quantity: Number
			}
		],
		totalAmount: Number,
		purchasedOn: {
				type: Date,
			default: new Date()
		}
})

module.exports = mongoose.model("Order", orderSchema);