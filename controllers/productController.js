const User = require('../models/User');
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const Orders = require("../models/Order");
const auth = require("../auth")

module.exports.addProduct = (prodModel) => {

	let newProduct = new Product ({
		name: prodModel.name,
		description: prodModel.description,
		price: prodModel.price
	});

	return newProduct.save().then((product, error) => {
		if(error) {
			return "Product creation failed"
		} else {
			return true
		}
	})
};

module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result
	})
}