const User = require('../models/User');
const Products = require("../models/Product");
const bcrypt = require("bcrypt");
const Orders = require("../models/Order");
const auth = require("../auth")


// User Registration
module.exports.registerUser = (reqBody) => {
	
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: (reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		
		if(error) {
			return "User registration fail"
		
		} else {
			return "User registration successful"
		}
	})
};

// Login Request

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false
		} else {
			 const isPasswordCorrect = bcrypt.compare(reqBody.password, result.password)

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
};

